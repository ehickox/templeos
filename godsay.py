#!/usr/bin/env python3
import requests
import random
from typing import List


beacon_resp = requests.get("https://beacon.nist.gov/beacon/2.0/pulse/last")
rand_seed = beacon_resp.json()['pulse']['outputValue']

words: List[str] = []

with open('Happy.TXT') as f:
    words = f.read().splitlines()
random.seed(rand_seed)
words = random.choices(words, k=32)

saying = ' '.join(words)
print(saying)

